import React, { useEffect, useState } from "react";
import { Title } from "./components/Title";

export const App = ({ name = "World" }) => {
  const test = " This is Ambikesh. Testing new APP";
  const url = "https://jsonplaceholder.typicode.com/todos/";
  const [todo, setTodo] = useState(null);
  useEffect(() => {
    getData()
  }, []);

  const getData = () => {
    fetch(url)
      .then(r => r.json())
      .then(data => {
        setTodo(data);
      });
  }
  return (
    <>
      <Title>
        Hello {name}! {test}
        <button onClick={() => getData()}> Refresh</button>
      </Title>
      {todo &&
        todo.map((d, i) => {
          return (
            <>
              {i+1}: {d.title} <br />
            </>
          );
        })}
    </>
  );
};
